.PHONY: asset

OUTPUT_NAME = marnix-presentatie
MAIN_NAME = presentatie

LATEX = ./makepdf.sh

all:
	make assets
	make text

assets: 
	echo placeholder

text: $(OUTPUT_NAME).pdf

$(OUTPUT_NAME).pdf:
	$(LATEX) $(MAIN_NAME).tex
	cp $(MAIN_NAME).pdf $(OUTPUT_NAME).pdf


